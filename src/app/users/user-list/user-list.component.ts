import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { UserService } from '../user.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DialogComponent } from '../../dialog-delete/dialog-delete.component';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';


@Component({
  selector: 'app-users',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit, AfterViewInit {
  dataSource = new MatTableDataSource([]);
  displayedColumns: string[] = ['name', 'email', 'createdAt', 'edit', 'delete'];
  @ViewChild(MatPaginator) paginator?: MatPaginator;

  constructor(public userService: UserService,
              public authService: AuthService,
              private dialog: MatDialog,
              private router: Router) {}

  /**
   * get data to the table
   */
  ngOnInit() {
    this.userService.users$.subscribe((users: any) => {
      this.dataSource.data = users;
    });
  }

  ngAfterViewInit() {
    if (this.paginator) {
      this.userService.loadUsers(0, this.paginator.pageSize);
    }
  }

  paginatorChange() {
    if (this.paginator) {
      this.userService.loadUsers(this.paginator.pageIndex, this.paginator.pageSize)
    }
  }

  editUser(userId?: string) {
    if (userId) {
      this.router.navigate([`/user-edit/${userId}`]);
    }
  }

  requestDeleteUser(userId?: string) {
    if (!userId) {
      return;
    }

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;

    const dialogRef = this.dialog.open(DialogComponent, dialogConfig);

    /**
     * after deleting a user, the users must be loaded again to upload data for pagination
     */
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.userService.deleteUser(userId).subscribe(() => {
          if (this.paginator) {
            this.userService.loadUsers(this.paginator.pageIndex, this.paginator.pageSize);
          }
        });
      }
    })
  }

}
