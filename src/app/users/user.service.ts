import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserDto } from '../interfaces/user.dto';
import { ListUsersDto } from '../interfaces/list-users.dto';
import { User } from '../models/user';
import { Observable, BehaviorSubject, map } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import { UpdateUserDto } from '../interfaces/update-user.dto';
import { NewUserDto } from '../interfaces/new-user.dto';
import { environment } from 'src/environments/environment';

export type userId = string;

@Injectable({providedIn: 'root'})
export class UserService {
  private users = new BehaviorSubject<User[]>([]);
  public users$: Observable<User[]> = this.users.asObservable();

  private currentUser = new BehaviorSubject<User | undefined>(undefined);
  public currentUser$: Observable<User | undefined> = this.currentUser.asObservable();

  public totalNumberOfUsers: number = 0;


  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute, private authService: AuthService) {
    this.authService.token$.subscribe((token?: string) => {
      if (token) {
        this.getCurrentUser();
      } else {
        this.currentUser.next(undefined);
      }
    });
  }

  /**
    * append param elements to user URL for pagination
   */
  loadUsers(pageIndex: number, pageSize: number) {
    this.http.get<ListUsersDto>(`${environment.apiUrl}/admin/user`,
    {
      params: new HttpParams().set('PageIndex', pageIndex).set('Limit', pageSize)
    })
    .subscribe((data: ListUsersDto) => {
      const userArray: User[] = [];

      data.results.forEach((userData: UserDto) => {
        const user = new User(userData);
        userArray.push(user);
      });
      this.users.next(userArray);

      this.totalNumberOfUsers = data.resultsLength;
    });
  }

  getUserData(userId: string): Observable<User> {
    return this.http.get<UserDto>(`${environment.apiUrl}/admin/user/${userId}`)
    .pipe(map(
      (data: UserDto): User => {
        return new User(data);
      }
    ));
  }

  saveUserData(userData: NewUserDto) {
    this.http.post(`${environment.apiUrl}/admin/user`, userData)
    .subscribe(data => {
        this.router.navigate(['']);
    });
  }

  updateUserData(userId: string, userData: UpdateUserDto) {
    this.http.put(`${environment.apiUrl}/admin/user/${userId}`, userData)
    .subscribe(data => {
        this.router.navigate(['']);
    });
  }

  deleteUser(userId: string): Observable<void> {
    return this.http.delete<void>(`${environment.apiUrl}/admin/user/${userId}`);
  }

  getCurrentUser() {
    this.http.get<UserDto>(`${environment.apiUrl}/user/me`)
    .subscribe((data: UserDto) => {
      this.currentUser.next(new User(data));
    });
  }


}
