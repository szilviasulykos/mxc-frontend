import { Component } from '@angular/core';
import { UserService } from '../user.service';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/models/user';
import { firstValueFrom } from 'rxjs';

  /**
   * this component shows the form with the user data
   * using the form for create a new user and also for editing an existing one
   */
@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent {
  public userForm: FormGroup;

  constructor(public userService: UserService, private route: ActivatedRoute) {
    this.userForm = new FormGroup({
      lastName: new FormControl('', [Validators.required]),
      firstName: new FormControl('', [Validators.required]),
      userName: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required, this.passwordRequirement]),
      phoneNumber: new FormControl(''),
      email: new FormControl('', [Validators.email])
    });
  }

/**
 * check if the user already exists
 * when yes, fill the form with the existing data
 * the Observable is changed to Promise, because we have/need just one result of the observable
 */
  async ngOnInit() {
    const userId = this.route.snapshot.params['id'];
    if (userId) {
      const user: User = await firstValueFrom(this.userService.getUserData(userId));
      this.fillFormWithUser(user);
      // by editing a user giving the password is not required
      this.userForm.get('password')?.setValidators([this.passwordRequirement]);
    }
  }

  /**
   * check if we would like to editing an existing user or create a new one
   * by editing userId is required
   */
  saveUserData() {
    const userId: string = this.route.snapshot.params['id'];
    if (userId) {
      this.userService.updateUserData(userId, {...this.userForm.value, userId});
    } else {
      this.userService.saveUserData(this.userForm.value);
    }
  }

  private fillFormWithUser(currentUser: User) {
    this.userForm.patchValue({
      lastName: currentUser.lastName ?? '',
      firstName: currentUser.firstName ?? '',
      userName: currentUser.userName ?? '',
      email: currentUser.email ?? '',
      phoneNumber: currentUser.phoneNumber ?? '',
    });
  }

  private passwordRequirement(control: AbstractControl): {[s: string]: boolean} | null {
    let lowerChar: boolean = false;
    let upperChar: boolean = false;

    /**
     * checks if the given password contains lowercase
     */
    if (control.value !== null && /[a-z]/.test(control.value)) {
      lowerChar = true;
    }

    /**
     * checks if the given password contains uppercase
     */
    if (control.value !== null && /[A-Z]/.test(control.value)) {
      upperChar = true;
    }

    if (lowerChar && upperChar) {
      return null;
    }
    return {'passwordIsWrong': true};
  }

}
