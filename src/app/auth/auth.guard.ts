import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, Router, UrlTree } from "@angular/router";
import { map, Observable, take } from "rxjs";
import { AuthService } from "./auth.service";


@Injectable({providedIn: 'root'})
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private authService: AuthService) {}

  /**
   * AuthGuard doesn't let user without token navigates to any of the pages
   * when user hasn't got a token, the guard navigates to the login page
   */
  canActivate(
    route: ActivatedRouteSnapshot,
    router: RouterStateSnapshot
  ): Observable<boolean | UrlTree> {
    return this.authService.token$
      .pipe(take(1),
      map((token) => {
        const isAuth = !!token;
        if (!isAuth) {
          return this.router.createUrlTree(['/login']);
        }
        return true;
      }));
    }

}
