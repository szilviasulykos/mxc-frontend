import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { TokenDto } from '../interfaces/token.dto';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({providedIn: 'root'})
export class AuthService {
  private token = new BehaviorSubject<string | undefined>(undefined);
  public token$: Observable<string | undefined> = this.token.asObservable();


  constructor(private http: HttpClient) {
    this.token.next(localStorage.getItem('token') ?? undefined);
  }

  /**
   * after login save the token in localStorage
   */
  login(username: string, password: string) {
    this.http.post<TokenDto>(`${environment.apiUrl}/identity/token`,
    {
      userName: username,
      password: password
    }
    )
    .subscribe((data: TokenDto) => {
      this.token.next(data.access_token);
      localStorage.setItem('token', data.access_token);
    });
  }

  /**
   * after logout remove token from localStorage
   */
  logout() {
    this.token.next(undefined);
    localStorage.removeItem('token');
  }

}
