import { Injectable } from '@angular/core';
import { HttpEventType, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { tap } from 'rxjs/operators';
import { LoadingIndicatorService } from '../dialog-loading/loading-indicator.service';

@Injectable()
export class LoadingInterceptorService implements HttpInterceptor{

  constructor(private loadingIndicatorService: LoadingIndicatorService) {}

  /**
   * react to the response of the http requests (close the dialog when request is ended)
   */
  intercept(req: HttpRequest<string>, next: HttpHandler) {
    this.loadingIndicatorService.loadingOn();

    return next.handle(req).pipe(
      tap((event: any) => {
        if (event.type === HttpEventType.Response) {
          this.loadingIndicatorService.loadingEnded();
        }
      })
    );
  }

}
