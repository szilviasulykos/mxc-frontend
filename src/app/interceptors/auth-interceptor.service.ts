import { Injectable } from '@angular/core';
import { HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { AuthService } from "../auth/auth.service";
import { switchMap, take } from 'rxjs';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor{

  constructor(private authService: AuthService) {}

  /**
   * append token and organisation id (required) to the header for authentication
   * using switchMap to get only one Observable as result of the intercept function
   * authService.token$ and next.handle are both Observables
   */
  intercept(req: HttpRequest<string>, next: HttpHandler) {
    return this.authService.token$
      .pipe(take(1),
      switchMap((token?: string) => {
        if(token) {
          const modifiedReq = req.clone(
            {
              headers: req.headers
                .append('Authorization', 'Bearer ' + token)
                .append('X-OrganisationId', '7065e94e-ace2-4846-9056-1638a97118e5')
            }
          );
          return next.handle(modifiedReq);
        } else {
          return next.handle(req);
        }
      }));
    }

}

