import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import { UserService } from '../users/user.service';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isLogged: boolean = false;

  constructor(public authService: AuthService, public userService: UserService, private router: Router) {}

  ngOnInit() {
    this.authService.token$.subscribe((token: string | undefined) => {
      this.isLogged = !!token;
    })
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }


}
