import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { LoadingDialogComponent } from './dialog-loading.component';

@Injectable({providedIn: 'root'})
export class LoadingIndicatorService {
  private reqNumber: number = 0;
  private dialogConfig = new MatDialogConfig();
  private dialogRef?: MatDialogRef<LoadingDialogComponent>;

  constructor(private dialog: MatDialog) {}

  /**
   * by http request shows a dialog, which closed automatically after the request is done
   * checks if a dialog is already open - when yes, doesn't open another, so there is always only one dialog on the screen
   */
  loadingOn() {
    this.dialogConfig.disableClose = true;
    if(this.reqNumber === 0) {
      this.dialogRef = this.dialog.open(LoadingDialogComponent, this.dialogConfig);
      this.reqNumber++;
    }
  }

  loadingEnded() {
    if (this.reqNumber !== 0) {
      this.dialogRef?.close();
      this.dialogRef = undefined;
      this.reqNumber--;
    }
  }

}
