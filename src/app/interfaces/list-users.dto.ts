import { UserDto } from "./user.dto";

export interface ListUsersDto {
  results: UserDto[];
  resultsLength: number;
}
