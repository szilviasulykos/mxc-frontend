export interface UserDto {
  id?:            string;
  userName:       string;
  email:          string;
  firstName:      string;
  lastName:       string;
  createdAt?:     string;
  phoneNumber?:   string;
}
