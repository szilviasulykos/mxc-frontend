export interface UpdateUserDto {
  userId:       string;
  email:        string;
  userName:     string;
  firstName:    string;
  lastName:     string;
  password:     string;
  phoneNumber:  string;
}
