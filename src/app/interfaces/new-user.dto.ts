export interface NewUserDto {
  userName:     string;
  email:        string;
  firstName:    string;
  lastName:     string;
  password:     string;
  phoneNumber:  string;
}
