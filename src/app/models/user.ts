import { UserDto } from '../interfaces/user.dto';

export class User {
  public id?: string;
  public userName: string;
  public firstName: string;
  public lastName: string;
  public email: string;
  public phoneNumber?: string;
  public createdAt?: Date;

  constructor(data: UserDto) {
    this.id = data.id;
    this.userName = data.userName;
    this.firstName = data.firstName;
    this.lastName = data.lastName;
    this.email = data.email;
    this.phoneNumber = data.phoneNumber;
    this.createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
  }

}
